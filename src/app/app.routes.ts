import { ModuleWithProviders }  from '@angular/core';
import { Routes,RouterModule }   from '@angular/router';
import { SintesisComponent }   from './sintesis/sintesis.component';

// Route Configuration
export const routes: Routes = [
  { path: 'sintesis', component: SintesisComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);