import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map'
declare var gapi: any;

@Injectable()
export class SintesisService {

  constructor(private http:Http) { }

  // Chart example
  drawSessions() {
    gapi.analytics.ready(function() {
      var form       = document.querySelector('.graph-session-container');
      var startDate = (<HTMLInputElement>form.querySelector('.start-date')).value != "" ? (<HTMLInputElement>form.querySelector('.start-date')).value : '30daysAgo';
      var endDate   = (<HTMLInputElement>form.querySelector('.end-date')).value != ""  ? (<HTMLInputElement>form.querySelector('.end-date')).value  : 'yesterday'; 
      var dataChart1 = new gapi.analytics.googleCharts.DataChart({
        query: {
          'ids': 'ga:69882213', // <-- Replace with the ids value for your view.
          'start-date':  startDate,
          'end-date': endDate,
          'metrics': 'ga:sessions,ga:users',
          'dimensions': 'ga:date'
        },
        chart: {
          'container': 'graph-sessions',
          'type': 'LINE',
          'options': {
            'width': '100%'
          }
        }
      });
      dataChart1.execute();
    });
  }
}