import { Component, OnInit } from '@angular/core';
import { AppService	} from '../app.service';
import { SintesisService  } from './sintesis.service';
import { routerTransition } from '../router.animations';

@Component({
  selector: 'app-sintesis',
  templateUrl: './sintesis.component.html',
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''},
  styleUrls: ['./sintesis.component.scss']
})
export class SintesisComponent implements OnInit {

	constructor (private appService: AppService,private sintesisService: SintesisService) {}

  ngOnInit() {
  	this.getData();
  }

  getData(){
  	this.appService.init();
    this.sintesisService.drawSessions();
  }

  filterData(event) {
    this.sintesisService.drawSessions();
  }
}
