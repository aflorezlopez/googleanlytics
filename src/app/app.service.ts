import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map'
declare var gapi: any;

@Injectable()
export class AppService {
	// URL to web API
	private serviceUrl = 'http://localhost:8000/connect';
	private token;    
	// Constructor
	constructor(private http:Http) { 
	
	}
	public init(){
		this.getToken();
	}

  // Load google api token
  private connect() {
  	var parent = this;
  	gapi.analytics.ready(function() {
			gapi.analytics.auth.authorize({
		    'serverAuth': {
		      'access_token': parent.token
		    }
		  });
		});
		return "true"; 
  }

  // Load google api token
  private getToken() {
    this.http.get(this.serviceUrl).map(res => res.json())
	    .subscribe(
	      data => this.token = data.token,
	      err => console.error('There was an error: ' + err),
	      () => this.connect()
	  );
	}
}