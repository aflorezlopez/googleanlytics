import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { SintesisComponent }   from './sintesis/sintesis.component';
import { AppService }   from './app.service';
import { SintesisService }   from './sintesis/sintesis.service';
import { routing } from './app.routes';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
  ],
  declarations: [
    AppComponent,
    SintesisComponent,
  ],
  providers: [
    AppService,
    SintesisService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
    // Module class
}