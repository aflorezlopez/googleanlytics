import { GoogleAnalyticsPage } from './app.po';

describe('google-analytics App', function() {
  let page: GoogleAnalyticsPage;

  beforeEach(() => {
    page = new GoogleAnalyticsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
